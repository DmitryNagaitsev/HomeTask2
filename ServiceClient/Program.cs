﻿using System;
using System.Collections.Generic;
using System.Linq;
using ServiceClient.ServiceReference1;

namespace ServiceClient
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var person1 = new Person() { FIO = "Иванов И. И.", Id = 1 };
            var personBooks = new List<Book>()
            {
                new Book
                {
                    Id = 10,
                    Author = "Loyd A. G.",
                    BookType = Book.BookTypes.Magazine,
                    Name = "Guide for fats",
                    Year = 322
                },
                new Book
                {
                    Id = 11,
                    Author = "Author11",
                    BookType = Book.BookTypes.Scientific,
                    Name = "Book1"
                },
            };

            var client = new LibraryServiceClient();
            client.JoinLibrary(person1);
            var books = client.GetBooks();
            //fail (5 or more books)
            client.RequestBooks(books);
            //success
            client.RequestBooks(new[] { books[0] });
            client.ReturnBooks(personBooks.ToArray());
            personBooks = client.Apply().ToList();
            personBooks.ForEach(x => Console.WriteLine(x.Name));
            client.Close();

            var person2 = new Person() { FIO = "Петров И. И.", Id = 2 };
            var client2 = new LibraryServiceClient();
            client2.JoinLibrary(person2);
            books = client2.GetBooks();
            client2.RequestBooks(new [] {books[0]});
            client2.Close();

            client2 = new LibraryServiceClient();
            client2.JoinLibrary(person2);
            client2.Close();
        }
    }
}

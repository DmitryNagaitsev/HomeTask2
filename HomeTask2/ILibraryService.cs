﻿using System.Collections.Generic;
using System.ServiceModel;

namespace HomeTask2
{
    [ServiceContract(SessionMode = SessionMode.Required)]
    public interface ILibraryService
    {
        [OperationContract]
        void JoinLibrary(Person person);

        [OperationContract(IsInitiating = false)]
        void ReturnBooks(List<Book> books);

        [OperationContract(IsInitiating = false)]
        ICollection<string> GetBooks();

        [OperationContract(IsInitiating = false)]
        void RequestBooks(List<string> names);

        [OperationContract(IsInitiating = false, IsTerminating = true)]
        ICollection<Book> Apply();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace HomeTask2
{
    public class LibraryService : ILibraryService
    {
        private Person _currentPerson;
        private ICollection<Book> _requestedBooks;
        private List<Book> _returnedBooks;
        private static readonly ICollection<Book> Library = new List<Book>()
        {
            new Book {Id=1, Author = "Author1", BookType = Book.BookTypes.Scientific, Name = "Book1"},
            new Book {Id=2, Author = "Author2", BookType = Book.BookTypes.Fiction, Name = "Book2"},
            new Book {Id=3, Author = "Author3", BookType = Book.BookTypes.Magazine, Name = "Book3"},
            new Book {Id=4, Author = "Author4", BookType = Book.BookTypes.Scientific, Name = "Book4"}
        };
        private static readonly Dictionary<int, ICollection<Book>> Orders = new Dictionary<int, ICollection<Book>>()
        {
            {1, new List<Book>()
                {
                    new Book {Id=10, Author = "Loyd A. G.", BookType = Book.BookTypes.Magazine, Name = "Guide for fats", Year = 322},
                    new Book {Id=11, Author = "Author11", BookType = Book.BookTypes.Scientific, Name = "Book1"},
                }
            },
            {2, new List<Book>() }
        };

        public void JoinLibrary(Person person)
        {
            _currentPerson = person;
            _requestedBooks = new List<Book>();
            _returnedBooks = new List<Book>();
            Console.WriteLine($"Hello, {person.FIO}! You have {Orders[_currentPerson.Id].Count} books");
        }

        public void ReturnBooks(List<Book> books)
        {
            _returnedBooks.AddRange(books);
        }

        public ICollection<string> GetBooks()
        {
            return Library.Select(x => x.Name).ToList();
        }

        public void RequestBooks(List<string> names)
        {
            if (names.Count + Orders[_currentPerson.Id].Count > 5)
            {
                Console.WriteLine("Sorry, only 5 books for 1 person! " +
                                  $"Now you have {Orders[_currentPerson.Id].Count} books.");
                return;
            }
            foreach (var name in names)
            {
                _requestedBooks.Add(Library.Single(x => x.Name.Equals(name)));
            }
        }

        public ICollection<Book> Apply()
        {
            foreach (var returned in _returnedBooks)
            {
                Orders[_currentPerson.Id].Remove(Orders[_currentPerson.Id]
                    .Single(x=>x.Name.Equals(returned.Name)));
                Library.Add(returned);
            }
            Console.WriteLine($"{_returnedBooks.Count} books returned.");

            foreach (var request in _requestedBooks)
            {
                Library.Remove(request);
                Orders[_currentPerson.Id].Add(request);
            }
            Console.WriteLine($"{_requestedBooks.Count} books taken.");
            Console.WriteLine($"Now you have {Orders[_currentPerson.Id].Count} books.");
            Console.WriteLine($"Bye, {_currentPerson.FIO}!");
            Console.WriteLine();
            return _requestedBooks;
        }
    }

    [DataContract]
    public class Book
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public BookTypes BookType { get; set; }

        public enum BookTypes
        {
            /// <summary>
            /// Журнал
            /// </summary>
            Magazine,
            /// <summary>
            /// Художественная книга
            /// </summary>
            Fiction,
            /// <summary>
            /// Научная
            /// </summary>
            Scientific
        }
    }

    public class Person
    {
        public int Id { get; set; }
        // ReSharper disable once InconsistentNaming
        public string FIO { get; set; }
    }
}